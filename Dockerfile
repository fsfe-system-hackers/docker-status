# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

FROM bitnami/apache:2.4

ARG LDAP_BIND_PW

# Install additional packages
USER 0
RUN install_packages docker.io

# Switch back to UID 1001
USER 1001

# Set up CGI and HTML files
COPY www/ /app/

COPY docker-gid /tmp/

# Set up Apache
# Enable required modules
RUN sed -i -r -e "s/#LoadModule cgid_module/LoadModule cgid_module/" \
              -e "s/#LoadModule authnz_ldap_module/LoadModule authnz_ldap_module/" \
              -e "s/#LoadModule ldap_module/LoadModule ldap_module/" \
              /opt/bitnami/apache/conf/httpd.conf
COPY docker-status.conf /vhosts/

# Change LDAP bind PW as root, then switch back
USER 0
# Grant UID 1001 access to docker socket
RUN useradd -M -u 1001 -g 0 -s /bin/bash user
# Make user 1001 part of socket group. Its gid can be different, so we take the
# one given to us by drone
RUN echo "socket:x:$(cat /tmp/docker-gid):user" >> /etc/group
# Set LDAP bind PW
RUN sed -i -r "s/LDAP_BIND_PW/${LDAP_BIND_PW}/" /vhosts/docker-status.conf

USER user
