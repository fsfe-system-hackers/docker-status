#!/bin/bash

# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

IFS='=&'
parm=($QUERY_STRING)

for ((i=0; i<${#parm[@]}; i+=2))
do
    declare var_${parm[i]}=${parm[i+1]}
done

echo "Content-type: text/html";
echo ""
echo "<html><body>"
echo "<a href=\"/cgi/logs.cgi?id=$var_id\">LOG output</a>"
echo "<pre>"
docker inspect $var_id
echo "</pre></body></html>"
