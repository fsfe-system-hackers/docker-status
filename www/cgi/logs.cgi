#!/bin/bash

# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

IFS='=&'
parm=($QUERY_STRING)

for ((i=0; i<${#parm[@]}; i+=2))
do
    declare var_${parm[i]}=${parm[i+1]}
done

echo "Content-type: text/html; charset=utf-8";
echo ""
echo "<!DOCTYPE html>"
echo "<html lang=\"en\">"
echo "<head>"
echo "<title>Logfile for $(docker inspect --format="{{.Config.Image}}" $var_id)</title>"
echo "<style>"
echo "span.DEBUG,    span.debug    {color: gray;   font-weight: bold}"
echo "span.INFO,     span.info     {color: green;  font-weight: bold}"
echo "span.NOTICE,   span.notice   {color: teal;   font-weight: bold}"
echo "span.WARNING,  span.warning  {color: navy;   font-weight: bold}"
echo "span.ERROR,    span.error    {color: purple; font-weight: bold}"
echo "span.CRITICAL, span.critical {color: maroon; font-weight: bold}"
echo "</style>"
echo "</head>"
echo "<body>"
echo "<pre>"
docker logs $var_id 2>&1 | sed -e "s/&/\&amp;/g; s/</\&lt;/g; s/>/\&gt;/g; s/\"/\&quot;/g; s/'/\&#39;/g" | sed -re 's/^.*[ [](DEBUG|INFO|NOTICE|WARNING|ERROR|CRITICAL|debug|info|notice|warning|error|critical)[]:].*$/<span class="\1">&<\/span>/'
echo "</pre>"
echo "</body>"
echo "</html>"
