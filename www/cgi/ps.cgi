#!/bin/bash

# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# Get GET variables and save them as $var_<variable>
IFS='=&'
parm=($QUERY_STRING)
for ((i=0; i<${#parm[@]}; i+=2))
do
    declare var_${parm[i]}=${parm[i+1]}
done

echo "Content-type: text/html";
echo ""
echo "<!DOCTYPE html>"
echo "<html><body>"
if [[ "${var_less}" -ne 1 ]]; then
  echo "<p>For less information and shorter loading time, browse the <a href=\"?less=1\">reduced view</a>.</p>"
else
  echo "<p>For more information at the cost of longer loading time, browse the <a href=\"?\">full view</a>.</p>"
fi
echo "<table><tbody>"
echo "<tr><th>NAME</th><th>STATUS</th><th>LOGS</th><th>VIRTUAL_HOST</th></tr>"

# Save list of all running docker containers separated by lines
ps=$(cat <<-END
$(docker ps --format "{{.Names}};{{.Status}};{{.ID}}" | sort)
END
)

while read -r line; do
  name=`echo $line | cut -d";" -f1`
  status=`echo $line | cut -d";" -f2`
  id=`echo $line | cut -d";" -f3`
  if [[ "${var_less}" -eq 1 ]]; then
    vhost="<em>truncated</em>"
  else
    vhost=`docker inspect -f "{{range .Config.Env}}{{println .}}{{end}}" ${id} | grep "VIRTUAL_HOST" | cut -d"=" -f2`
  fi

  echo "
<tr>
<td><a id=${name} href=\"/cgi/view.cgi?id=${id}\">${name}</a></td>
<td>${status}</td>
<td><a href=\"/cgi/logs.cgi?id=${id}\">Logs</a></td>
<td>${vhost}</td>
</tr>"

done < <(echo "$ps")

echo "</tbody></table>"
echo "</body></html>"
